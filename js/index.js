require('intersection-observer');
import LazyLoad from 'vanilla-lazyload';
import { LuminousGallery } from 'luminous-lightbox';
import Masonry from 'masonry-layout';

function setupGradient(image) {
  var palette = JSON.parse(image.getAttribute("data-palette").replace(/\'/g, '"'))
  image.style.backgroundImage = "-webkit-linear-gradient("+ palette[0] +" , "+ palette[1] +" , "+ palette[2] +")";
}

function init() {

    var grid = document.querySelector('.grid');
    
    // shuffle dom element order
    // https://stackoverflow.com/questions/7070054/javascript-shuffle-html-list-element-order
    for (var i = grid.children.length; i >= 0; i--) {
      grid.appendChild(grid.children[Math.random() * i | 0]);
    }

    // make some images bigger
    const images = document.querySelectorAll('.grid-item');
    images.forEach(i => {
      var rating = i.getAttribute("data-rating")
      switch(rating) {
        case "5": i.className += " grid-item--width3"; break;
        case "4": if(Math.random() < 0.4) i.className += " grid-item--width2"; break;
        case "3": if(Math.random() < 0.25) i.className += " grid-item--width2"; break;
        default: ;
      }

      var ratio = parseFloat(i.getAttribute("data-ratio"))
      if(ratio > 3) {
        i.className += " grid-item--width3";
      }
      setupGradient(i);

      // hover effect
      // i.onmouseover = function()   {
      //   hoverItem = this
      // };
      // i.onmouseout = function()   {
      //   hoverItem.style.transform = "";
      //   hoverItem = undefined
      // };
    });

    // hover effect
    // …continued

    

    // let ε = 0.1
    // let translateScale = scalePow().domain([-0.5-ε, 0.5+ε]).range([4, -4])
    // let perspectiveScale = scaleLinear().domain([-0.5, 0, 0.5]).range([350, 348, 350])

    // document.onmousemove = function(event) {
    //   if(hoverItem == undefined) return

    //   var rect = hoverItem.getBoundingClientRect();
    //   var centerX = rect.left + (rect.right - rect.left) / 2
    //   var centerY = rect.top + (rect.bottom - rect.top) / 2

    //   var δX = event.clientX - centerX
    //   var δY = event.clientY - centerY

    //   var δxNormalized = δX / (rect.right - rect.left)
    //   var δyNormalized = δY / (rect.bottom - rect.top)

    //   console.log("δX", δX, δxNormalized, translateScale(δxNormalized))

    //   var perspective = perspectiveScale(Math.max(δxNormalized, δyNormalized))
    //   var size = 1.01
    //   var rotateX = δxNormalized
    //   var rotateY = δyNormalized
      

    //   // var range = Math.min((rect.right - rect.left), (rect.bottom - rect.top)) * 0.08

    //   // translateScale.range([range, -range])
    //   // var translateX = translateScale(δxNormalized)
    //   // var translateY = translateScale(δyNormalized)
    //   // // var size = sizeScale(Math.max(δxNormalized, δyNormalized))

    //   hoverItem.style.transform = `perspective(${perspective}px) scale(${size}) rotateX(${rotateX}deg) rotateY(${rotateY}deg)`;
    //   var img  = hoverItem.querySelector('img')
    //   console.log("img", img)

    //   img.style.transform = `scale(1.025) translateX(${translateScale(δxNormalized)}px) translateY(${translateScale(δyNormalized)}px)`;
    // }
    

    new Masonry( grid, {
      columnWidth: '.grid-sizer',
      itemSelector: '.grid-item'
    }).layout();

    new LazyLoad({threshold: 0});
    new LuminousGallery(document.querySelectorAll(".grid-item"));
}

init()


