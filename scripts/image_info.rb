require 'RMagick'
require 'mini_exiftool'

include Magick

# https://blog.kellishaver.com/image-color-analysis-with-rmagick/
def pixel_to_color_string(pixel, colorspace='hex') 

    # Look at the red, green, and blue histogram values and divide by 256 
    # - the number value associated with each color when specified as RGB.
    r_int = pixel.red / 256
    g_int = pixel.green / 256
    b_int = pixel.blue / 256

    # Now take those newly calculated colors and convert them to hexadecimal strings 
    # using ruby's built in to_s method, to determine our HEX color values for each color channel.
    r_hex = r_int.to_s(16)
    g_hex = g_int.to_s(16)
    b_hex = b_int.to_s(16)

    # Sometiems our resulting HEX is only a single character, rather than two, 
    # but our fully returned HEX value needs to be 6 characters in length. 
    # To solve this, we concatanate the string with itself to pad it unless it's already long enough.
    # This is the equvilent of taking a shortened #FFF in your CSS and expanding it to its complete form as #FFFFFF.
    r_hex += r_hex unless r_hex.length == 2 
    g_hex += g_hex unless g_hex.length == 2
    b_hex += b_hex unless b_hex.length == 2

    # return the string corresponding to the requested color space
    colorspace == 'rgb' ? "rgb(#{r_int},#{g_int},#{b_int})" : "##{r_hex}#{g_hex}#{b_hex}"
end

def get_info(directory, tags) 
    result = []
    Dir.each_child(directory) { |name| 
        # Allowing for all kinds of image formats.
        # In practive, we're actually using but jpegs.
        next if not /\.(gif|jpg|jpeg|tiff|png|webp)$/.match(name)
        image_path = File.expand_path(name, directory)
        exif = MiniExiftool.new image_path
        info = {}
        info[:Name] = name
        info[:Id] = name.gsub(/\..*$/, '')

        EXIF_TAGS.each do |tag|
        info[tag.to_sym] = exif[tag]
        end
        result.push info
    }
    return result
end

def write_info(directory, info)
    File.open(directory, 'w') { |file| 
        file.write(JSON.generate(info)) 
    }
end

def downsample(src_directory, info)
    p "downsampling #{info.size} images. This might take a while…"

    info.each() {|image_info| 
        # load image
        path = File.expand_path(image_info[:Name], src_directory)
        image = Image.read(path).first

        # extract palette
        # http://www.imagemagick.org/discourse-server/viewtopic.php?f=1&t=25086&hilit=color+palete
        # convert input.jpg +dither -colors 16 -unique-colors -scale 2110% filename_palette.gif
        num_colorz = 4
        palette = image
            .quantize(number_colors=num_colorz, colorspace=RGBColorspace, dither=NoDitherMethod, tree_depth=0, measure_error=false)
            .unique_colors()
            .get_pixels(0, 0, num_colorz, 1)
            .map { |pixel| pixel_to_color_string( pixel, 'rgb' )} # extract the rgb colors from the pixels
            .slice(1..(num_colorz-1)) # we cut away the first entry, since thins is a boring darkish color anyway…
            .reverse # we reverse here since js likes it better that way. I —for one— welcome my new js overlords
        
        # sStuff that palette data into that image_info object
        # It ain't pretty, but that's how ruby rolls.
        # At least we return the info object when we're done
        image_info[:Palette] = palette

        # The wee tiny vm used by gitlab to deploy the site craps out when extracting all them images.
        # so we just force a garbage collection (after some tidying up) after each run.
        # doing this makes the script —oh so— much slower
        # but that's ok… we call the script once in a while
        # in a regular environment this would not be necessary
        palette = nil
        image = nil
        path = nil
        GC.start 
    }

    # leave this one alone!
    # it will be shown as 'external' during the build.
    # we all like some feedback.
    p "DONE downsampling"
    
    return info
end

path = File.expand_path(ARGV[0])
EXIF_TAGS = [
            # "Orientation",
            "CreateDate",
            "Rating",
            # "Title",
            # "Description",
            # "Subject",
            "ImageWidth",
            "ImageHeight"
            # "FaceElementSelected",
            # "FacesDetected",
            # "FacePositions",
            # "NumFaceElements",
            # "FaceElementTypes",
            # "FaceElementPositions"
            ]

partial_info = get_info(path, EXIF_TAGS)
complete_info = downsample(path, partial_info)
write_info(File.expand_path('./data/images.json'), complete_info)
