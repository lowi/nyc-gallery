# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

activate :autoprefixer do |prefix|
  prefix.browsers = "last 2 versions"
end

# activate :livereload

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

activate :external_pipeline,
  name: :webpack,
  command: build? ? './node_modules/webpack/bin/webpack.js --config webpack.config.js --bail' : './node_modules/webpack/bin/webpack.js --config webpack.config.js --watch -d',
  source: ".tmp/dist",
  latency: 1


activate :external_pipeline,
  name: :imagez,
  command: 'ruby ./scripts/image_info.rb ./source/images/nyc',
  source: "./source/images/nyc",
  latency: 1

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

# helpers do
#   def some_helper
#     'Helping'
#   end
# end

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

configure :build do
  activate :minify_css
  
  # webpack should be taking care of minimizing the js
  # activate :minify_javascript

  # gitlab deployment from here on…
  set :build_dir, 'public'

  # baseurl for GitLab Pages 
  # obacht. MUST be empty for gitlab to find the thing.
  # unclear why that is.
  set :base_url, "" 
  
  # Use relative URLs
  activate :relative_assets 
end
